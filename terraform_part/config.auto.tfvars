profile = "default"
region  = "us-east-1"

name            = "none"
namespace       = "genesis"
environment     = "test"
stage           = "none"

server_subnet   = "subnet-0877cb29"
vpc_id          = "vpc-f977b584"

server_key      = "yzaiveliev"

web_ec2_type    = "t2.micro"
db_ec2_type     = "t2.micro"

web_volume = [
    {
      volume_type = "gp2"
      volume_size = 10
    },
  ]
db_volume  = [
    {
      volume_type = "gp2"
      volume_size = 10
    },
  ]