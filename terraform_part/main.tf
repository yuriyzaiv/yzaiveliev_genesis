provider "aws" {
  region  = var.region
  profile = var.profile
}


###
# Web server
###

module "web_server" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "${module.labels.namespace}-web-server"
  instance_count         = 1

  ami                    = var.web_ec2_ami
  instance_type          = var.web_ec2_type
  key_name               = var.server_key

  vpc_security_group_ids = ["${aws_security_group.web_server.id}"]
  subnet_id              = var.server_subnet

  root_block_device      = var.web_volume

  tags = {
    Terraform   = "true"
    Environment = "genesis"
  }
}

###
# DB server
###

module "db_server" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "${module.labels.namespace}-db-server"
  instance_count         = 1

  ami                    = var.db_ec2_ami
  instance_type          = var.db_ec2_type
  key_name               = var.server_key

  vpc_security_group_ids = ["${aws_security_group.db_server.id}"]
  subnet_id              = var.server_subnet

  root_block_device      = var.db_volume

  tags = {
    Terraform   = "true"
    Environment = "genesis"
  }
}