module "labels" {
  source        = "git::https://github.com/cloudposse/terraform-terraform-label.git?ref=tags/0.5.0"
  namespace   = var.namespace
  name        = var.name
  attributes  = var.attributes
}
