###
# Provider
###

variable "profile" {
    type    = string
    default = "default"
}

variable "region" {
    type = string
}

###
# Labels
###

variable "namespace" {
    type = string
}

variable "environment" {
    type = string
    default = ""
}

variable "name" {
    type = string
}

variable "attributes" {
    type = list(string)
	default = []
}

variable "delimiter" {
    type = string
    default = "-"
}

variable "stage" {
    type = string
    default = ""
}

###
# Security group
###

variable "vpc_id" {
    type = string
    default = ""
}



###
# EC2 variables
### 
variable "web_ec2_ami" {
    type = string
    default = "ami-0817d428a6fb68645"
}

variable "db_ec2_ami" {
    type = string
    default = "ami-0817d428a6fb68645"
}

variable "web_ec2_type" {
    type = string
    default = "t2.micro"
}

variable "db_ec2_type" {
    type = string
    default = "t2.micro"
}

variable "web_volume" {
    type = list(map(string))
    default = [
    {
      volume_type = "gp2"
      volume_size = 8
    },
  ]
}

variable "db_volume" {
    type = list(map(string))
    default = [
    {
      volume_type = "gp2"
      volume_size = 8
    },
  ]
}

variable "server_key" {
    type = string
    default = ""
}

variable "server_subnet" {
    type = string
    default = ""
}