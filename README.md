!!! At first run of Ansible you can get error below, to resolve it run config secondly(it's can be improved)
fatal: [ip]: FAILED! => {"changed": false, "msg": "The PyMySQL (Python 2.7 and Python 3.X) or MySQL-python (Python 2.X) module is required."}

1. You need to enter your values into terraform var file config.auto.tfvars and run terraform init, plan and apply commands.
2. In the ansible config you need to enter IPs into hosts file and your own values into group_vars -> all file 
3. After this you can execute ansible-playbook -i hosts main.yml in ansible_part folder
4. To test this configuration you can add "ip   example.com" line into /etc/hosts file